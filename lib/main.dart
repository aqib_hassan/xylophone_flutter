// ignore_for_file: prefer_const_constructors

import 'dart:ffi';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  void playSound(int soundNumber) {
    final player = AudioPlayer();
    player.play(AssetSource('note$soundNumber.wav'));
  }

  Expanded paintButtons(int buttonNumber, Color color) {
    return Expanded(
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(backgroundColor: color),
        onPressed: () {
          playSound(buttonNumber);
        },
        child: Text(''),
      ),
    );
  }

  List<Widget> columnChiledrens() {
    List<Widget> childrens = [];
    for (var i = 0; i < colors.length; i++) {
      childrens.add(paintButtons(i + 1, colors[i]));
    }
    return childrens;
  }

  static const colors = [
    Colors.red,
    Colors.yellow,
    Colors.green,
    Colors.blue,
    Colors.pink,
    Colors.teal,
    Colors.orange
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: columnChiledrens(),
        )),
      ),
    );
  }
}
